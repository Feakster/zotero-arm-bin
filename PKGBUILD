# Maintainer: Feakster <feakster at posteo dot eu>
# Contributor: Juanma Hernandez <juanmah at gmail dot com>
# Contributor: Zhaofeng Li <hello at zhaofeng dot li>

### Notes ###
# - This PKGBUILD replaces the original Zotero binaries with Firefox ones. Apparently, this is also
#   performed by the official packaging script:
#   https://github.com/zotero/zotero-standalone-build/blob/master/fetch_xulrunner.sh

### Info ###
pkgname=zotero-arm-bin
pkgver=6.0.27
_ffver=60.0.2 # 60.9.0esr?
_ubuver=18.04.1
pkgrel=1
pkgdesc='Zotero Standalone is a free, easy-to-use tool to help you collect, organize, cite, and share your research sources (ARM Binaries)'
arch=('aarch64' 'armv7h') # 'x86_64'
url='http://www.zotero.org/download'
license=('AGPL3')
depends=('dbus-glib' 'gtk3' 'hicolor-icon-theme' 'libxt' 'nss' 'startup-notification')
provides=('zotero')
optdepends=(
    'zotero-xpdf: PDF indexing'
    'poppler-data: PDF indexing'
)
source=(
    'zotero.desktop'
    "Zotero-${pkgver}_linux_x86_64.tar.bz2::https://www.zotero.org/download/client/dl?channel=release&platform=linux-x86_64&version=$pkgver"
)
source_aarch64=("firefox-$_ffver-aarch64.deb::http://launchpadlibrarian.net/373617392/firefox_$_ffver+build1-0ubuntu0.${_ubuver}_arm64.deb")
source_armv7h=("firefox-$_ffver-armv7h.deb::http://launchpadlibrarian.net/373609086/firefox_$_ffver+build1-0ubuntu0.${_ubuver}_armhf.deb")
# source_x84_64=("firefox-$_ffver-x86_64.deb::https://ftp.mozilla.org/pub/firefox/releases/${_ffver}esr/linux-x86_64/en-US/firefox-${_ffver}esr.tar.bz2")
b2sums=('d67654b2e7ced24f1ecad24372bd2c890f5778b02623c399cc92ee698802927aa984b27e7ce5fe12e95eb5c921fc69c5e404087c34feaae4a73994d96bdc2b72'
        '0c324253d0f6b4385c54979f6806ee964fe4e349cbd18d5b67d93a68e6b4b0214ac52e29a2a30a3a301f300023fb980dc2386d827cec6d0aca2de08f0fd50d37')
b2sums_aarch64=('aa20c65f6bd2a87092b9931f3b9fe66023563d817af379b7e9c7f752f680862b6ae587af332d6255a16107aee47b778a4752fdf817b530cce20dea7bd64a0d0a')
b2sums_armv7h=('917b5821f006e0ab47f8c6d46771511dc826d52fb915dc796f0380bee57748b36d50ca39e93238414f4b4da370378f65d38f6148b1d59eb9c370bfc6c4c3bb19')

### Prepare ###
prepare() {

    ## Extract Firefox ##
    tar -xf "$srcdir"/data.tar.xz ./usr/lib/firefox --strip-components=3 -C "$srcdir"
    rm "$srcdir"/{debian-binary,control.tar.xz,data.tar.xz}

    ## Remove Zotero Binaries ##
    rm "$srcdir"/Zotero_linux-x86_64/**.{so,chk}
    rm "$srcdir"/Zotero_linux-x86_64/gtk2/*
    rm -r "$srcdir"/Zotero_linux-x86_64/gmp-clearkey
    rm "$srcdir"/Zotero_linux-x86_64/{updater,minidump-analyzer}
    rm -r "$srcdir"/Zotero_linux-x86_64/{pdftotext,pdfinfo,poppler-data} # Poppler Binaries.

    ## Remove Uneccessary Files ##
    rm "$srcdir"/Zotero_linux-x86_64/{zotero.desktop,set_launcher_icon} # Desktop launcher.

    ## Disable App Update ##
    sed -i \
        's/^pref("app.update.enabled", true);/pref("app.update.enabled", false);/' \
        "$srcdir"/Zotero_linux-x86_64/defaults/preferences/prefs.js

    ## Disable Persistent Shell When Launching Zotero ##
    sed -i -r \
        's|^("\$CALLDIR/zotero-bin" -app "\$CALLDIR/application.ini" "\$@")|exec \1|' \
        "$srcdir"/Zotero_linux-x86_64/zotero

}

### Package ###
package() {

    ## Create Installation Directory Structure ##
    install -d "$pkgdir"/usr/{bin,lib/zotero,share/applications}

    ## Move Core Contents to "$pkgdir"/usr/lib/zotero ##
    cp -RLT "$srcdir"/Zotero_linux-x86_64 "$pkgdir"/usr/lib/zotero

    ## Install Zotero Icons to a Standard Location ##
    for res in 16 32 48 256; do
        install -Dm0644 "$pkgdir"/usr/lib/zotero/chrome/icons/default/default$res.png "$pkgdir"/usr/share/icons/hicolor/${res}x$res/apps/zotero.png
    done
    rm -r "$pkgdir"/usr/lib/zotero/chrome

    ## Replace Deleted Zotero Binaries With Firefox Ones ##
    install -Dm0644 "$srcdir"/firefox/*.{so,chk} "$pkgdir"/usr/lib/zotero/
    install -Dm0644 "$srcdir"/firefox/gtk2/*.so "$pkgdir"/usr/lib/zotero/gtk2/
    install -Dm0755 "$srcdir"/firefox/plugin-container "$pkgdir"/usr/lib/zotero/plugin-container
    install -Dm0755 "$srcdir"/firefox/firefox "$pkgdir"/usr/lib/zotero/zotero-bin
    install -Dm0644 "$srcdir"/firefox/dependentlibs.list "$pkgdir"/usr/lib/zotero/dependentlibs.list

    ## Link Binaries From zotero-xpdf (optional) ##
    ln -fs /usr/lib/zotero-xpdf/pdfinfo "$pkgdir"/usr/lib/zotero/pdfinfo
    ln -fs /usr/lib/zotero-xpdf/pdftotext "$pkgdir"/usr/lib/zotero/pdftotext
    ln -fs /usr/share/poppler "$pkgdir"/usr/lib/zotero/poppler-data

    ## Symlink Main Binary ##
    ln -fs /usr/lib/zotero/zotero "$pkgdir"/usr/bin/zotero

    ## Install Desktop Launcher ##
    install -Dm0644 "$srcdir"/zotero.desktop "$pkgdir"/usr/share/applications/zotero.desktop

}
